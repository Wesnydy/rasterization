#ifndef _MYGL_H_
#define _MYGL_H_

#include "definitions.h"
#include "primitives.h"

#include <math.h>

//*****************************************************************************
// Considerando a largura da tela,
// calcula a posição de um pixel, dada as suas coordenadas (x,y),
// colorindo a posição com a cor especificada.
//*****************************************************************************
void put_pixel(const int x, const int y, const Colors &color) {
  int mempos = ( x * 4 ) + ( y * IMAGE_WIDTH * 4 );
  FBptr[mempos] = color.R_;
  FBptr[mempos + 1] = color.G_;
  FBptr[mempos + 2] = color.B_;
  FBptr[mempos + 3] = color.A_;
}

//*****************************************************************************
// Calcula o valor do incremento de cada componente RGBA do pixel
//*****************************************************************************
Colors color_increment(const Colors &first, const Colors &last, const float linesize) {
  Colors incremental_value;

  incremental_value.R_ = (last.R_ - first.R_)/linesize;
  incremental_value.G_ = (last.G_ - first.G_)/linesize;
  incremental_value.B_ = (last.B_ - first.B_)/linesize;

  return incremental_value;
}

void draw_line(const Line &line) {
  Point initial_p = line.get_initial_point();
  Point final_p   = line.get_final_point();

  int posx  = initial_p.get_posx();
  int posy  = initial_p.get_posy();

  int fposx = final_p.get_posx();
  int fposy = final_p.get_posy();

  bool xdecres; // x decrescente
  bool ydecres; // y decrescente

  posx > fposx ? xdecres = true : xdecres = false;
  posy > fposy ? ydecres = true : ydecres = false;

  int dx = abs(fposx - posx);
  int dy = abs(fposy - posy);
  int decision;
  int inc_e;
  int inc_ne;

  Colors pixel_color = initial_p.get_color();

  float line_size = sqrt(pow(dx,2)+pow(dy,2));
  Colors c_increment = color_increment(initial_p.get_color(), final_p.get_color(), line_size);

  //***************************************************************************
  // Modificação para desenhar retas com ângulo de 90 graus
  //***************************************************************************
  if (dx == 0) { // Verifica se a reta é invariante no eixo x
    int posi = posy;
    while (posi <= fposy) {
      put_pixel(posx, posi, pixel_color);
      pixel_color.R_ += c_increment.R_;
      pixel_color.G_ += c_increment.G_;
      pixel_color.B_ += c_increment.B_;
      ydecres ? posi-- : posi++;
    }
    return;
  }

  //***************************************************************************
  // Modificação para desenhar retas nos 8 octantes
  //***************************************************************************
  if (dy > dx) {
    decision = (2 * dx) - dy;
    inc_e  = 2 * dx;
    inc_ne = 2 * (dx - dy);
    while (posy != fposy) {
      if (decision <= 0){
        decision += inc_e;
        ydecres ? posy-- : posy++;
      }
      else {
        decision += inc_ne;
        ydecres ? posy-- : posy++;
        xdecres ? posx-- : posx++;
      }
      put_pixel(posx, posy, pixel_color);
      pixel_color.R_ += c_increment.R_;
      pixel_color.G_ += c_increment.G_;
      pixel_color.B_ += c_increment.B_;
    }
    return;
  }

  decision = (2 * dy) - dx;
  inc_e  = 2 * dy;
  inc_ne = 2 * (dy - dx);
  while (posx != fposx) {
    if (decision <= 0){
      decision += inc_e;
      xdecres ? posx-- : posx++;
    }
    else {
      decision += inc_ne;
      xdecres ? posx-- : posx++;
      ydecres ? posy-- : posy++;
    }
    put_pixel(posx, posy, pixel_color);
    pixel_color.R_ += c_increment.R_;
    pixel_color.G_ += c_increment.G_;
    pixel_color.B_ += c_increment.B_;
  }
}

void draw_triangle(const Point &verta, const Point &vertb, const Point &vertc) {
  Line *line1 = new Line(verta, vertb);
  Line *line2 = new Line(verta, vertc);
  Line *line3 = new Line(vertb, vertc);

  draw_line(*line1);
  draw_line(*line2);
  draw_line(*line3);
}

#endif // _MYGL_H_
