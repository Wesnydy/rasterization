#ifndef _PRIMITIVES_H_
#define _PRIMITIVES_H_

#include "definitions.h"
#include <iostream>

struct Colors {
  float R_;
  float G_;
  float B_;
  float A_;
};

class Point {
  public:
    Point() {}
    Point(const int px, const int py, const Colors &color)
     : posx_(px), posy_(py), color_(color) {}
    ~Point() {}

    int get_posx() const {
      return posx_;
    }

    int get_posy() const {
      return posy_;
    }

    Colors get_color() const {
      return color_;
    }

  private:
    int posx_;
    int posy_;
    Colors color_;
};

class Line {
  public:
    Line() {}
    Line(const Point &begin, const Point &end)
      : begin_(begin), end_(end) {}
    ~Line() {}

    Point get_initial_point() const {
      return begin_;
    }

    Point get_final_point() const {
      return end_;
    }

  private:
    Point begin_;
    Point end_;
};

#endif // _PRIMITIVES_H_
