# Introdução à computação gráfica - Trabalho 1 #

* Período 2016.1
* Turma do prof. Christian Azambuja Pagot

### Compilando e executando o projeto ###

 Para compilar e executar o projeto, basta digitar os seguintes comandos no terminal:
```sh
    $ make
    $ make run
```

### Contribuidores ###

* Wesnydy Ribeiro <wesnydy1@gmail.com>