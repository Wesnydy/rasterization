#include "main.h"

//-----------------------------------------------------------------------------

void MyGlDraw(void)
{
	//***************************************************************************
	// Rasterização de ponto,
	// Exibe quatro quadrados na seguinte ordem de cor: RED, GREEN, BLUE, WHITE
	//***************************************************************************
	// Colors pcolor;
	// for (int i = 0; i < IMAGE_HEIGHT; i+=8) {
	// 	for (int j = 0; j < IMAGE_WIDTH; j+=8) {
	// 		pcolor.R_ = 255;
	// 		pcolor.G_ = 0;
	// 		pcolor.B_ = 0;
	// 		pcolor.A_ = 255;
	//
	// 		if(i > 256) {
	// 			pcolor.R_ = 0;
	// 			pcolor.G_ = 255;
	// 		}
	//
	// 		if(j > 256) {
	// 			pcolor.R_ = 0;
	// 			pcolor.G_ = 0;
	// 			pcolor.B_ = 255;
	// 		}
	//
	// 		if( i > 256 && j > 256){
	// 			pcolor.R_ = 255;
	// 			pcolor.G_ = 255;
	// 			pcolor.B_ = 255;
	// 		}
	//
	// 		put_pixel(i, j, pcolor);
	// 	}
	// }

	//***************************************************************************
	// Rasterização de linha,
	//***************************************************************************
	// Colors cor1 = {0, 0, 255, 255};
	// Colors cor2 = {0, 255, 255, 255};
	//
	// Point ponto1(0, 0, cor1);
	// Point ponto2(510, 511, cor2);
	// Line *linha1 = new Line(ponto1, ponto2);
	//
	// Point ponto3(511, 0, cor1);
	// Point ponto4(0, 510, cor2);
	// Line *linha2 = new Line(ponto3, ponto4);
	//
	// Point ponto5(256, 0, cor1);
	// Point ponto6(256, 511, cor2);
	// Line *linha3 = new Line(ponto5, ponto6);
	//
	// Point ponto7(0, 256, cor1);
	// Point ponto8(512, 256, cor2);
	// Line *linha4 = new Line(ponto7, ponto8);
	//
	// Point ponto9(256, 256, cor1);
	// Point ponto10(0, 128, cor2);
	// Line *linha5 = new Line(ponto9, ponto10);
	//
	// Point ponto11(256, 256, cor1);
	// Point ponto12(0, 384, cor2);
	// Line *linha6 = new Line(ponto11, ponto12);
	//
	// Point ponto13(256, 256, cor1);
	// Point ponto14(128, 511, cor2);
	// Line *linha7 = new Line(ponto13, ponto14);
	//
	// Point ponto15(256, 256, cor1);
	// Point ponto16(384, 511, cor2);
	// Line *linha8 = new Line(ponto15, ponto16);
	//
	// Point ponto17(256, 256, cor1);
	// Point ponto18(511, 384, cor2);
	// Line *linha9 = new Line(ponto17, ponto18);
	//
	// Point ponto19(256, 256, cor1);
	// Point ponto20(511, 128, cor2);
	// Line *linha10 = new Line(ponto19, ponto20);
	//
	// Point ponto21(256, 256, cor1);
	// Point ponto22(384, 0, cor2);
	// Line *linha11 = new Line(ponto21, ponto22);
	//
	// Point ponto23(256, 256, cor1);
	// Point ponto24(128, 0, cor2);
	// Line *linha12 = new Line(ponto23, ponto24);

	// draw_line(*linha1);
	// draw_line(*linha2);
	// draw_line(*linha3);
	// draw_line(*linha4);
	// draw_line(*linha5);
	// draw_line(*linha6);
	// draw_line(*linha7);
	// draw_line(*linha8);
	// draw_line(*linha9);
	// draw_line(*linha10);
	// draw_line(*linha11);
	// draw_line(*linha12);

	//***************************************************************************
	// Rasterização de triangulo,
	//***************************************************************************
	//Triangulo 1
	Colors cor5 = {255, 0, 0, 255};
	Colors cor6 = {0, 255, 0, 255};
	Colors cor7 = {0, 0, 255, 255};
	Point vertexa(128,128, cor5);
	Point vertexb(128,256, cor6);
	Point vertexc(384,256, cor7);

	// //Triangulo 2
	// Point vertexa(256,128, cor5);
	// Point vertexb(178,256, cor6);
	// Point vertexc(335,256, cor7);

	// Triangulo 3
	// Point vertexa(256,128, cor5);
	// Point vertexb(198,300, cor6);
	// Point vertexc(315,300, cor7);

	// Triangulo 4
	// Point vertexa(80,128, cor5);
	// Point vertexb(200,256, cor6);
	// Point vertexc(384,256, cor7);

	draw_triangle(vertexa, vertexb, vertexc);

}

//-----------------------------------------------------------------------------
int main(int argc, char **argv)
{
	// Inicializações.
	InitOpenGL(&argc, argv);
	InitCallBacks();
	InitDataStructures();

	// Ajusta a função que chama as funções do mygl.h
	DrawFunc = MyGlDraw;

	// Framebuffer scan loop.
	glutMainLoop();

	return 0;
}
