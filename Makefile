OK        = \33[32;5mOK\033[m
ERROR     = \33[31;5mERROR\33[m
BIN				= cgprog
CC				= g++
GLFLAGS		= -lglut -lGLU -lGL
INCLUDES	= -I include
MAIN			= src/main.cpp

all:
	@echo -n "Compiling... "
	@$(CC) ${MAIN} ${GLFLAGS} ${INCLUDES} -o ${BIN} && echo "${OK}" || (echo "${ERROR}"; exit 1;)

clean:
	@@echo -n "Cleaning... "
	@rm cgprog && echo "${OK}" || (echo "${ERROR}"; exit 1;)

run:
	@./${BIN} || (echo "${ERROR}"; exit 1;)
